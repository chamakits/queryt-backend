package com.queryt.datastore;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import org.junit.Test;

import com.google.gson.JsonObject;
import com.queryt.datastore.dbms.GenericJDBCInformation;
import com.queryt.formatter.JsonFormatter;

public class HsqlDbTesting
{

	@Test
	public void test() throws SQLException
	{
		GenericJDBCInformation gen = new GenericJDBCInformation();
		DBDataRetriever dbDataRetriever = DBDataRetriever.getTargetDBInstance();
		ResultSet resultSet = dbDataRetriever
				.query("SELECT * FROM PUBLIC.TESTING");
		ResultSetMetaData metadata = resultSet.getMetaData();
		resultSet.next();
		System.out.println(metadata.getColumnCount());
		int type = metadata.getColumnType(1);
		System.out.println("type:"+type);
		System.out.println("type-String:"+gen.getTypeAsString(type));
		System.out.println(resultSet.getString(1));
	}

	@Test
	public void testingSQLTypeIntToString() throws IllegalArgumentException,
			IllegalAccessException
	{
		Field[] fields = Types.class.getFields();
		for (Field field : fields)
		{
			System.out.println(field.toGenericString());
			System.out.println(field.getInt(0));
			System.out.println(field.getName());
		}
		GenericJDBCInformation gen = new GenericJDBCInformation();
		
		for(String str : gen.getAllTypesAsString())
		{
			System.out.println(str);
		}
	}
	
	@Test
	public void testingResultSetToString() throws SQLException
	{
		GenericJDBCInformation gen = new GenericJDBCInformation();
		DBDataRetriever dbDataRetriever = DBDataRetriever.getTargetDBInstance();
		ResultSet resultSet = dbDataRetriever
				.query("SELECT * FROM PUBLIC.TESTING");
		while(resultSet.next())
		{
			System.out.println("Result:"+resultSet.getString(1));
		}
		
		//Choosing to print an int column as string, works.
		resultSet = dbDataRetriever
				.query("SELECT * FROM PUBLIC.INT_VALUES");
		while(resultSet.next())
		{
			System.out.println("Result:"+resultSet.getString(1));
		}
		
	}
	
	@Test
	public void testingJsonFormattingOfResultSet() throws SQLException
	{
		GenericJDBCInformation gen = new GenericJDBCInformation();
		DBDataRetriever dbDataRetriever = DBDataRetriever.getTargetDBInstance();
		ResultSet resultSet = dbDataRetriever
				.query("SELECT * FROM PUBLIC.TESTING");
		
		JsonObject json = JsonFormatter.resultSetToJson(resultSet);
		System.out.println(json.toString());
	}
	
	@Test
	public void testingJsonSchemaAndValues()
	{
		GenericJDBCInformation gen = new GenericJDBCInformation();
		DBDataRetriever dbDataRetriever = DBDataRetriever.getTargetDBInstance();
		ResultSet resultSet = dbDataRetriever
				.query("SELECT * FROM PUBLIC.TESTING");
		
		JsonObject rowsAsJson = JsonFormatter.resultSetToJson(resultSet);
		JsonObject rowSchemaAsJson =null;
		try
		{
			rowSchemaAsJson = JsonFormatter.resultSetMetadataToJsonSchema(resultSet.getMetaData());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		JsonObject rowsAndSchema = new JsonObject();
		rowsAndSchema.add("schema", rowSchemaAsJson);
		rowsAndSchema.add("data", rowsAsJson);
		System.out.println(rowsAndSchema.toString());
	}

}
