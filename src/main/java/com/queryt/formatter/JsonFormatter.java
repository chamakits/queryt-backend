package com.queryt.formatter;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.commons.lang3.ObjectUtils;

import spark.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class JsonFormatter
{
	public static JsonObject resultSetToResponseJson(ResultSet resultSet,Response response)
	{
		JsonObject rowsAsJson = JsonFormatter.resultSetToJson(resultSet);
		JsonObject rowSchemaAsJson = null;
		try
		{
			rowSchemaAsJson = JsonFormatter
					.resultSetMetadataToJsonSchema(resultSet.getMetaData());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			response.status(400);
			response.body(JsonFormatter.errorMessageJson(
					"There was an issue retrieving the metadata for the query requested, "
							+ "and cannot determine the schema as a result.  "
							+ "Please contact IT suport").getAsString());
		}

		JsonObject rowsAndSchema = new JsonObject();
		rowsAndSchema.add("schema", rowSchemaAsJson);
		rowsAndSchema.add("data", rowsAsJson);
		
		JsonObject queryResult= new JsonObject();
		
		queryResult.add("queryResult", rowsAndSchema);
		return queryResult;
	}
	
	public static JsonObject resultSetToJson(ResultSet resultSet)
	{
		String colStr="column";
		int counter = 1;
		
//		JsonObject resultRow;
		JsonArray resultRow = new JsonArray();
		JsonArray resultsArray = new JsonArray();
		
		try
		{
			int columnCount = resultSet.getMetaData().getColumnCount();
			while(resultSet.next())
			{
				resultRow = new JsonArray();
				for(counter=1;counter <= columnCount; ++counter)
				{
					resultRow.add(new JsonPrimitive(ObjectUtils.firstNonNull(resultSet.getString(counter),"NULL_ENTRY")));
//					resultRow.add(new JsonPrimitive((String)null));
				}
				resultsArray.add(resultRow);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JsonObject results = new JsonObject();
		results.add("rows", resultsArray);
		return results;
	}

	public static JsonObject resultSetMetadataToJsonSchema(
			ResultSetMetaData metaData)
	{
		JsonObject schema= new JsonObject();
		JsonObject schemaEntry = new JsonObject();
		JsonArray arrayOfSchemaEntries= new JsonArray();
		try
		{
			int columnCount = metaData.getColumnCount();
			for(int counter = 1 ; counter <=columnCount; ++counter)
			{
				schemaEntry = new JsonObject();
				schemaEntry.addProperty("type", metaData.getColumnTypeName(counter));
				schemaEntry.add("name", new JsonPrimitive(metaData.getColumnName(counter)));
				arrayOfSchemaEntries.add(schemaEntry);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		schema.add("columns", arrayOfSchemaEntries);
		return schema;
	}

	public static JsonObject errorMessageJson(String string)
	{
		JsonObject json = new JsonObject();
		json.addProperty("error", string);
		return json;
	}

}
