package com.queryt;

import static spark.Spark.options;
import static spark.Spark.post;
import static spark.Spark.setPort;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.queryt.datastore.DBDataRetriever;
import com.queryt.formatter.JsonFormatter;

public class JsonService
{
	private static final DBDataRetriever dbDataRetriever;
	private static final DBDataRetriever metaDataRetriever;
	static
	{
		dbDataRetriever = DBDataRetriever.getTargetDBInstance();
		metaDataRetriever = DBDataRetriever.getMetadataDBInstance();
	}

	public static void main(String[] args)
	{
		setPort(30101);

		post(new Route("/sample/get-data-rows")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				String originHeader= request.headers("Origin");
				response.header("Access-Control-Allow-Origin", originHeader);
				return processDataRowRequest(request, response);
				// response.body();
			}
		});

		options(new Route("/sample/get-data-rows")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				processOptions(request, response);
				// return "Hello World!";
				return "";
			}
		});
		
		post(new Route("/api/solve-query")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				String originHeader= request.headers("Origin");
				response.header("Access-Control-Allow-Origin", originHeader);
				return processQuery(request, response);
				// response.body();
			}
		});

		options(new Route("/api/solve-query")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				processOptions(request, response);
				// return "Hello World!";
				return "";
			}
		});
		
		
		post(new Route("/api/update-query")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				String originHeader= request.headers("Origin");
				response.header("Access-Control-Allow-Origin", originHeader);
				return processUpdate(request, response);
				// response.body();
			}
		});

		options(new Route("/api/update-query")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				processOptions(request, response);
				// return "Hello World!";
				return "";
			}
		});

		post(new Route("/api/query-history")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				System.out.println("QUERY HISTORY FOUND EY MATE");
				String originHeader= request.headers("Origin");
				response.header("Access-Control-Allow-Origin", originHeader);
				return getHistory(request, response);
				// response.body();
			}
		});

		options(new Route("/api/query-history")
		{
			@Override
			public Object handle(Request request, Response response)
			{
				processOptions(request, response);
				// return "Hello World!";
				return "";
			}
		});		
		
		
	}

	
	protected static Object processUpdate(Request request, Response response)
	{
		dbDataRetriever.processUpdate(request);
		String string = "{}";
		System.out.println(string);
		response.body(string);
		dbDataRetriever.closeConnection();
		return string;
	}


	protected static Object getHistory(Request request, Response response)
	{
		// TODO Auto-generated method stub
		ResultSet resultSet = metaDataRetriever.query("SELECT * FROM QUERY_HISTORY_TABLE ORDER BY GROUP_ID, CURR_ORDER");
		
		JsonObject queryResult = JsonFormatter.resultSetToResponseJson(
				resultSet, response);
		
		queryResult = mergeHistoryRows(queryResult);
		
		String string = queryResult.toString();
		System.out.println(string);
		response.body(string);
		metaDataRetriever.closeConnection();
		
		return string;
	}


	private static JsonObject mergeHistoryRows(JsonObject queryResult)
	{
		JsonElement jsonData = queryResult.get("queryResult").getAsJsonObject().get("data");
		
		JsonArray rows = jsonData.getAsJsonObject().get("rows").getAsJsonArray();
		
		int prevGroupId = -1;
		int currGroupId = 0;
		String currentQueryString = "";
		String currentCommentString ="";
		JsonArray newJsonRow = new JsonArray();
		JsonArray newJsonRows = new JsonArray();
		
		for(JsonElement current : rows)
		{
			JsonArray currRow = current.getAsJsonArray();
			
			currGroupId = Integer.valueOf(currRow.get(1).getAsString(), 10);
			
			if(currGroupId == prevGroupId)
			{
				currentQueryString+=currRow.get(3).getAsString();
				currentCommentString+=currRow.get(4).getAsString();
			}
			else
			{
				newJsonRow.add(new JsonPrimitive(currGroupId+""));
				newJsonRow.add(new JsonPrimitive(currentQueryString));
				newJsonRow.add(new JsonPrimitive(currentCommentString));
				
				System.out.println("ROW:");
				System.out.println(newJsonRow);
				newJsonRows.add(newJsonRow);
				currentQueryString=currRow.get(3).getAsString();
				currentCommentString = currRow.get(4).getAsString();
				newJsonRow = new JsonArray();
			}
			prevGroupId = currGroupId;
		}
		newJsonRow.add(new JsonPrimitive(currGroupId+""));
		newJsonRow.add(new JsonPrimitive(currentQueryString));
		newJsonRow.add(new JsonPrimitive(currentCommentString));
		
		newJsonRows.add(newJsonRow);

		JsonElement newJsonMergedColumns = makeNewMergedColumns();
		//.add("columns", newJsonMergedColumns);
		//add("rows", newJsonRows);
		
		
		JsonObject newData = new JsonObject();
		newData.add("rows", newJsonRows);
//		newData.add("schema",newJsonMergedColumns);
		
		JsonObject newQueryRes = new JsonObject();
		newQueryRes.add("data", newData);
		newQueryRes.add("schema", newJsonMergedColumns);
		
		JsonObject newWrapper = new JsonObject();
		newWrapper.add("queryResult", newQueryRes);
		
		System.out.println("New jsonData");
		System.out.println(newWrapper.toString());
		
		return newWrapper;
	}


	private static JsonObject makeNewMergedColumns()
	{
		JsonArray newColumns = new JsonArray();

		JsonObject newColObj = new JsonObject();
		newColObj.addProperty("name", "GROUP_ID");
		newColObj.addProperty("type", "BIGINT");
		newColumns.add(newColObj);

		newColObj = new JsonObject();
		newColObj.addProperty("name", "QUERY");
		newColObj.addProperty("type", "VARCHAR");
		newColumns.add(newColObj);

		newColObj = new JsonObject();
		newColObj.addProperty("name", "COMMENTS");
		newColObj.addProperty("type", "VARCHAR");
		newColumns.add(newColObj);		
		
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("columns", newColumns);
		
		return jsonObject;
	}


	protected static Object processQuery(Request request, Response response)
	{
		ResultSet resultSet = dbDataRetriever.processQuery(request);
		JsonObject queryResult = JsonFormatter.resultSetToResponseJson(
				resultSet, response);
		String string = queryResult.toString();
		System.out.println(string);
		response.body(string);
		dbDataRetriever.closeConnection();
		return string;
	}

	protected static String processDataRowRequest(Request request,
			Response response)
	{
		ResultSet resultSet = dbDataRetriever.getResultSetRequested(request);
//////////START
//		JsonObject rowsAsJson = JsonFormatter.resultSetToJson(resultSet);
//		JsonObject rowSchemaAsJson = null;
//		try
//		{
//			rowSchemaAsJson = JsonFormatter
//					.resultSetMetadataToJsonSchema(resultSet.getMetaData());
//		}
//		catch (SQLException e)
//		{
//			e.printStackTrace();
//			response.status(400);
//			response.body(JsonFormatter.errorMessageJson(
//					"There was an issue retrieving the metadata for the query requested, "
//							+ "and cannot determine the schema as a result.  "
//							+ "Please contact IT suport").getAsString());
//		}
//
//		JsonObject rowsAndSchema = new JsonObject();
//		rowsAndSchema.add("schema", rowSchemaAsJson);
//		rowsAndSchema.add("data", rowsAsJson);
//		
//		JsonObject queryResult= new JsonObject();
//		
//		queryResult.add("queryResult", rowsAndSchema);
//////////END	
		JsonObject queryResult = JsonFormatter.resultSetToResponseJson(resultSet, response);
		
		String string = queryResult.toString();
		System.out.println(string);
		response.body(string);
		dbDataRetriever.closeConnection();
		return string;
	}

	protected static Object listJson(Request request, Response response)
	{
		JsonElement jsonInput = null;
		try
		{
			jsonInput = verifyExpectedJsonInput(request, response, "input");
		}
		catch (JsonArgumentException e)
		{
			return e.getJsonErr();
		}
		Gson gson = new Gson();
		String inputString = gson.fromJson(jsonInput, String.class);
		List<String> outputList = getStringAsInfoArray(inputString);
		String originHeader = request.headers("Origin");
		response.header("Access-Control-Allow-Origin", originHeader);
		return stringListToJsonArray("output", outputList);
	}

	private static String stringListToJsonArray(String fieldName,
			List<String> outputList)
	{
		JsonArray arr = new JsonArray();

		JsonObject jo = new JsonObject();
		for (String str : outputList)
		{
			arr.add(new JsonPrimitive(str));
		}
		// {"fieldName":arr}
		jo.add(fieldName, arr);
		return jo.toString();
	}

	private static List<String> getStringAsInfoArray(String inputString)
	{
		List<String> retList = new ArrayList<String>();
		for (int count = 0; count < inputString.length(); ++count)
		{
			String toAdd = String.format("Input[%d] is \'%c\'.", count,
					inputString.charAt(count));
			retList.add(toAdd);
		}
		return retList;
	}

	private static JsonElement verifyExpectedJsonInput(Request request,
			Response response, String fieldName) throws JsonArgumentException
	{
		JsonElement jelement = new JsonParser().parse(request.body());
		JsonObject jobject = null;
		try
		{
			jobject = jelement.getAsJsonObject();
		}
		catch (IllegalStateException jexception)
		{
			response.status(400);
			throw new JsonArgumentException("output",
					"Expected json input, but found:\r\n" + request.body());
		}

		JsonElement jFieldElement = jobject.getAsJsonPrimitive(fieldName);
		if (jFieldElement == null)
		{
			response.status(400);
			throw new JsonArgumentException("output", String.format(
					"Expected json filed with name \"%s\", got instead:\r\n%s",
					jobject.toString()));
		}

		return jFieldElement;
	}

	protected static Object helloJson(Request request, Response response)
	{

		JsonElement jName = null;
		try
		{
			jName = verifyExpectedJsonInput(request, response, "name");
		}
		catch (JsonArgumentException e)
		{
			return e.getJsonErr();
		}
		String originHeader = request.headers("Origin");
		response.header("Access-Control-Allow-Origin", originHeader);
		JsonObject jo = processJsonNameInput(request, jName);
		return jo.toString();
	}

	private static JsonObject processJsonNameInput(Request request,
			JsonElement jName)
	{
		Gson gson = new Gson();
		JsonObject jsonObject = new JsonObject();
		Session session = request.session(true);
		String countStr = session.attribute("count");
		System.out.println(countStr);
		if (countStr != null)
		{
			int count = Integer.parseInt(countStr, 10);
			session.attribute("count", (count + 1) + "");
			jsonObject.addProperty(
					"message",
					"Welcome back " + gson.fromJson(jName, String.class)
							+ ".  You've been here "
							+ session.attribute("count") + " times.");
			return jsonObject;
		}
		session.attribute("count", 1 + "");
		jsonObject.addProperty("message",
				"Hello " + gson.fromJson(jName, String.class));
		return jsonObject;
	}

	protected static Object debugPost(Request request, Response response)
	{
		// TODO Auto-generated method stub
		return null;
	}

	protected static Object processOptions(Request request,
			Response response) {
		String accessControlMethodHeader = request.headers("Access-Control-Request-Method");
		if(!accessControlMethodHeader.equals("POST"))
		{
			response.status(400);
			return "Expected only \"POST\" method on accessControlRequest";
		}
		String accessControlRequestHeadersHeader = request.headers("Access-Control-Request-Headers");
		String originHeader= request.headers("Origin");
		
		response.header("Access-Control-Allow-Origin", originHeader);
		response.header("Access-Control-Allow-Methods", "POST");
		response.header("Access-Control-Allow-Headers", accessControlRequestHeadersHeader);
		
		return "";
	}

}
