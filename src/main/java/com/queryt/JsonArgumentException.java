package com.queryt;

import com.google.gson.JsonObject;

public class JsonArgumentException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7750778089572237990L;
	private JsonObject jsonErr;

	public JsonObject getJsonErr()
	{
		return jsonErr;
	}

	public JsonArgumentException(String fieldName, String errName)
	{
		this.jsonErr = new JsonObject();
		this.jsonErr.addProperty(fieldName, errName);
	}

}
