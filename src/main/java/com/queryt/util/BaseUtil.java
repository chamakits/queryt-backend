package com.queryt.util;

public class BaseUtil
{
	public static int max(int val1, int val2)
	{
		return val1>val2 ? val1 : val2;
	}
	
	public static int min(int val1, int val2)
	{
		return val1<val2 ? val1 : val2;
	}
}
