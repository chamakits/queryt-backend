package com.queryt.datastore;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

import spark.Request;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.queryt.datastore.dbms.DbQueryRetriever;
import com.queryt.datastore.dbms.GenericJDBCInformation;
import com.queryt.util.BaseUtil;

/***
 * Will be implementing this right now with simple straighforward SQL, but will
 * hopefully move it to MYBATIS later on.
 * 
 * @author chamakits
 * 
 */
public class DBDataRetriever
{
	private static final int QUERY_ROW_SIZE = 100;

	public static GenericJDBCInformation jdbcInfo = new GenericJDBCInformation();

	private BasicDataSource basicDataSource;
	private Connection connection;
	private boolean isMetadataRetriever=false;

	// TODO Unhardcode this.
	private DBDataRetriever(String propertiesPath)
	{
		Properties prop = new Properties();
		InputStream in = System.class.getResourceAsStream(propertiesPath);
		try
		{
			prop.load(in);
			in.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.basicDataSource = new BasicDataSource();
		this.basicDataSource.setDriverClassName(prop.getProperty("db.driver"));
		this.basicDataSource.setUsername(prop.getProperty("db.username"));
		this.basicDataSource.setPassword(prop.getProperty("db.password"));
		this.basicDataSource.setUrl(prop.getProperty("db.dbUrl"));
	}

	public static DBDataRetriever getTargetDBInstance()
	{
		return new DBDataRetriever("/db/h2-connection.properties");
	}
	
	public static DBDataRetriever getMetadataDBInstance()
	{
		DBDataRetriever dbDataRetriever = new DBDataRetriever("/db/h2-query-store-connection.properties");
		dbDataRetriever.isMetadataRetriever = true;
		return dbDataRetriever;
	}
	
	public void update(String query)
	{
		try
		{
			if (connection == null)
			{
				this.connection = this.basicDataSource.getConnection();
			}
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ResultSet query(String query)
	{
		ResultSet resultSet = null;
		try
		{
			if (connection == null)
			{
				this.connection = this.basicDataSource.getConnection();
			}
			Statement statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultSet;
	}

	public ResultSet getResultSetRequested(Request request)
	{
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(request.body()).getAsJsonObject();
		DbQueryRetriever retriever = new DbQueryRetriever();

		String query = retriever.getQueryFromTag(jsonObject.get("queryt-tag")
				.getAsString());
		
		return this.query(query);
	}

	public void closeConnection()
	{
		if (this.connection != null)
		{
			try
			{
				this.connection.close();
				this.connection = null;
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public ResultSet processQuery(Request request)
	{
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(request.body()).getAsJsonObject();
		String query = jsonObject.get("query").getAsString();
		
		//TODO This should really be abstracted a bit higher up, like in the JsonService.  Easier to do it here FOR NOW.  REALLY should change it though.
		if(!this.isMetadataRetriever)
		{
			this.storeQuery(query);
		}
		
		return this.query(query);
	}

	private DBDataRetriever metadatDBDataRetriever;
	private void storeQuery(String query)
	{
		if(this.metadatDBDataRetriever == null)
		{
			this.metadatDBDataRetriever = DBDataRetriever.getMetadataDBInstance();
		}
		String formattedQueryInsert;
		try
		{
			formattedQueryInsert = DBDataRetriever.prepareQueryStringForInsertAsSQLString(query, metadatDBDataRetriever);
			System.out.println(formattedQueryInsert);
			this.metadatDBDataRetriever.update(formattedQueryInsert);
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String prepareQueryStringForInsertAsSQLString(String query, DBDataRetriever metadataDBDataRetriever) throws SQLException
	{
		ResultSet maxGroupResultSet = metadataDBDataRetriever.query("SELECT MAX(GROUP_ID) FROM PUBLIC.QUERY_HISTORY_TABLE");
		
		query = query.replaceAll("'", "''");
		
		long maxGroup =0;
		if(maxGroupResultSet.next())
		{
			maxGroup = maxGroupResultSet.getLong(1);
		}
		++maxGroup;
		
		String groupInsert = String.format("(%d,", maxGroup);
		
		
		//TODO Use a string builder, as it might add up.
		String insertQuery = 
				"INSERT INTO PUBLIC.QUERY_HISTORY_TABLE " +
				"(GROUP_ID,CURR_ORDER,QUERY, COMMENTS) " +
				"VALUES ";

		StringBuilder builder = new StringBuilder();
		builder.append(insertQuery);
		
		String remainderQuery = query;
		
		int counter = 1;
		String currentSubString = "";
		int currentZero = 0;
		int nextZero = 0;
		int queryLength = query.length();	
		
		do{
			int min = BaseUtil.min(queryLength - nextZero, DBDataRetriever.QUERY_ROW_SIZE);
			nextZero = currentZero+min;
			
//			System.out.println("Min: "+ min);
//			System.out.println("queryLength :"+ queryLength );
//			System.out.println("nextZero: "+ nextZero);
//			System.out.println("currentZero: " + currentZero);
//			System.out.println("queryLength - nextZero: " + (queryLength - nextZero));

			currentSubString = 
					remainderQuery.substring
					(currentZero, nextZero);
			
			builder.append(groupInsert);
			builder.append(String.format("%d,'%s',''), ",counter,currentSubString));
			++counter;
			currentZero = nextZero;
			
		}while(nextZero < queryLength);
		//TODO This while loop can be greatly improved by calculating how many times I have to chop it up, and then not reassigning every time.
		
		return builder.toString();
	}

	public void initQueryStoreTable()
	{
		this.update
		("CREATE TABLE IF NOT EXISTS PUBLIC.QUERY_HISTORY_TABLE" 
				+ "("
				+ "    QUERY_ID BIGINT IDENTITY," 
				+ "    GROUP_ID BIGINT,"
				+ "    CURR_ORDER BIGINT," 
				+ "    QUERY VARCHAR(100),"
				+ "    COMMENTS VARCHAR(100)" 
				+ ")");
		// TODO Auto-generated method stub
		
	}

	public void processUpdate(Request request)
	{
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = parser.parse(request.body()).getAsJsonObject();
		String query = jsonObject.get("query").getAsString();
		
		System.out.println("Query:"+query);
		//TODO This should really be abstracted a bit higher up, like in the JsonService.  Easier to do it here FOR NOW.  REALLY should change it though.
		if(!this.isMetadataRetriever)
		{
			this.storeQuery(query);
		}
		this.update(query);
		
	}

}
