package com.queryt.datastore.dbms;

import java.util.HashMap;
import java.util.Map;

/**
 * This should be done in a smarter way....eventually.
 * @author chamakits
 *
 */
public class DbQueryRetriever
{
	
	private Map<String, String> querytTagToQueryMap;

	public DbQueryRetriever(){
		this.querytTagToQueryMap = new HashMap<>();
		this.querytTagToQueryMap.put("int_values", "SELECT * FROM PUBLIC.INT_VALUES fetch limit 100000");
	};

	public String getQueryFromTag(String querytTag)
	{
		return this.querytTagToQueryMap.get(querytTag);
	}

}
