package com.queryt.datastore.dbms;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Types;
import java.util.Collection;
import java.util.HashMap;

//Might not need this after discovering 'metaData.getColumnTypeName'
public class GenericJDBCInformation
{	
	private HashMap<Integer, String> intTypeToString;

	public GenericJDBCInformation()
	{
		this.intTypeToString = new HashMap<Integer,String>();
		Field[] fields = Types.class.getFields();
		for (Field field : fields)
		{
			if(Modifier.isFinal(field.getModifiers()) && Modifier.isStatic(field.getModifiers()))
			{
				try
				{
					this.intTypeToString.put(field.getInt(null), field.getName());
				}
				catch (IllegalArgumentException | IllegalAccessException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public String getTypeAsString(int type)
	{
		return this.intTypeToString.get(type);
	}
	
	public Collection<String> getAllTypesAsString()
	{
		return this.intTypeToString.values();
	}
}
