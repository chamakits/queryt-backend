package com.queryt.datastore.h2.init;

import java.sql.SQLException;

import org.h2.tools.Server;

import com.queryt.datastore.DBDataRetriever;

public class StartH2Server
{
	
	public static String TMP_STRING="TMPSTRING";
	
	//com.queryt.datastore.h2.init.StartH2Server
	public static void main(String[] args) throws SQLException, InterruptedException
	{
		Server server = new Server();
		
		ServerStarter starter = new ServerStarter(server);
		DBIniter initer = new DBIniter(server);
		
		Thread starterThread = new Thread(starter);
		Thread initThread = new Thread(initer);
		
		starterThread.start();
		initThread.start();
	}

}

class ServerStarter implements Runnable{
	private Server server;

	public ServerStarter(Server server)
	{
		this.server = server;
	}
	
	@Override
	public void run()
	{
		try
		{
			server.runTool("-webDaemon","-tcpAllowOthers");
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}

class DBIniter implements Runnable{
	private Server server;

	public DBIniter(Server server)
	{
		this.server = server;
	}
	
	@Override
	public void run()
	{
		try
		{
			Thread.sleep(10000);
		} catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("About to init.");
		DBIniter.dbInit();
	}
	
	private static void dbInit()
	{
		DBDataRetriever dbDataRetriever = DBDataRetriever.getMetadataDBInstance();
		
		DBDataRetriever targetRetriever = DBDataRetriever.getTargetDBInstance();
		targetRetriever.update
		("CREATE TABLE IF NOT EXISTS PUBLIC.SOME_TBL" +
				"(CD VARCHAR(80)," +
				"VAL INT)");
		
		targetRetriever.update
		("INSERT INTO PUBLIC.SOME_TBL(CD, VAL)"+
			    "values"+
			    "('SOME CODE', 1),"+
			    "('Abc', 2),"+
			    "('Say woot', 3),"+
			    "('Hello world', 4),"+
			    "('Zomg lol', 5)");
		
		dbDataRetriever.initQueryStoreTable();
		
	}

}
